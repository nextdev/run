<?php
namespace nextdev\Run;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;

class Application extends BaseApplication
{
    /**
     * Mapping of flags to commands.
     */
    protected array $triggerOptions = [
        '--list-config' => 'list-config',
    ];

    /**
     * Set a command trigger.
     *
     * @param $option Option including dashes.
     */
    public function setCommandTrigger(
        string $option,
        ?string $commandName
    ): void {
        if ($option[0] !== '-') {
            $option = (strlen($option) > 1 ? '--' : '-') . $option;
        }

        $this->triggerOptions[$option] = $commandName;
    }

    protected function getCommandName(
        InputInterface $input
    ): ?string {
        if (!$input->hasParameterOption('command', true)) {
            foreach ($this->triggerOptions as $option => $commandName) {
                if ($input->hasParameterOption($option, true)) {
                    return $commandName;
                }
            }
        }

        return $input->getOption('command');
    }

    protected function getDefaultInputDefinition(): InputDefinition
    {
        $definition = parent::getDefaultInputDefinition();
        $definition->setArguments();

        $definition->addOption(new InputOption(
            'command',
            null,
            InputOption::VALUE_REQUIRED,
            'Which application command to execute',
            Command\RunCommand::getDefaultName(),
        ));

        foreach ($this->triggerOptions as $option => $commandName) {
            $descr = $this->get($commandName)->getDescription() ?: sprintf('Run %s command', $commandName);

            $definition->addOption(new InputOption(
                substr($option, substr($option, 0, 2) == '--' ? 2 : 1),
                $option[2] === '-' ? substr($option, 1) : null,
                InputOption::VALUE_NONE,
                $descr,
            ));
        }

        return $definition;
    }

    protected function getDefaultCommands(): array
    {
        return array_merge(parent::getDefaultCommands(), [
            new Command\ListConfigCommand(),
            new Command\RunCommand(),
        ]);
    }
}
