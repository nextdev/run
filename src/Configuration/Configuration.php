<?php
namespace nextdev\Run\Configuration;

use InvalidArgumentException;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('run');

        $builder->getRootNode()->children()
            ->arrayNode('jobs')
                ->defaultValue([])
                ->useAttributeAsKey('name')
                ->normalizeKeys('false')
                ->variablePrototype()
                    ->beforeNormalization()
                        ->ifString()
                        ->then(function (string $v): array {
                            return [$v];
                        })
                    ->end()
                    ->validate()
                        ->always(function ($a) {
                            if (!\is_array($a)) {
                                throw new InvalidArgumentException(sprintf('Invalid type'));
                            }

                            foreach ($a as $c) {
                                if (!\is_string($c) && !\is_array($c)) {
                                    throw new InvalidArgumentException(sprintf('Invalid type'));
                                }
                            }

                            return $a;
                        })
                    ->end()
                ->end()
            ->end()
            ->floatNode('status-interval')
                ->defaultValue(0.2)
                ->validate()
                    ->always(function (float $v) {
                        if ($v <= 0) {
                            throw new InvalidArgumentException(sprintf('Status interval needs to be greater zero'));
                        }

                        return $v;
                    })
                ->end()
            ->end()
        ->end();

        return $builder;
    }
}
