<?php
namespace nextdev\Run\Command;

use SplObjectStorage;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;

final class RunCommand extends BaseCommand
{
    protected static $defaultName = "run";

    public function configure(): void
    {
        parent::configure();
        $definition = $this->getDefinition();

        $definition->addOption(new InputOption(
            'param',
            'p',
            InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
            'Parameter to pass to a child process',
        ));
        $definition->addOption(new InputOption(
            'interval',
            'i',
            InputOption::VALUE_REQUIRED,
            'Interval to check for process status',
        ));

        $definition->addArgument(new InputArgument(
            'job',
            InputArgument::OPTIONAL,
            'The job to run',
        ));
    }

    public function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $this->readConfig($input, $output);

        $jobName = $input->getArgument('job');

        if (!isset($jobName)) {
            if (\count($this->config['jobs']) === 0) {
                $this->getErrput($output)->writeln('No jobs configured.');
            }

            foreach ($this->config['jobs'] as $jobName => $job) {
                $output->writeln($jobName);
            }

            return 0;
        }

        $job = $this->config['jobs'][$jobName] ?? null;
        if (!isset($job)) {
            throw new InvalidArgumentException(sprintf('job "%s" does not exist', $jobName));
        }

        /** @var Process[] */
        $processes = [];

        /** @var OutputInterface[] */
        $put = ['out' => $output, 'err' => $this->getErrput($output)];

        $lastWrite = null;
        $lastWritePerPut = new SplObjectStorage();

        foreach ($job as $k => $command) {
            $processes[$k] = \is_array($command) ?
                new Process($command) :
                Process::fromShellCommandline($command);

            $processes[$k]->setInput("");

            $processes[$k]->start(
                (function (string $pipe, string $content) use ($processes, $put, &$lastWrite, $lastWritePerPut) {
                    /** @var Process $this */
                    static $k;
                    if (!isset($k)) {
                        $k = \array_search($this, $processes, true);
                        if (\is_numeric($k)) {
                            \preg_match('/^(?: (\'") [^\'"]+ (\\1) | \S+ )*/x', $this->getCommandLine(), $m);
                            $k = $k . '-' . basename(strtr($m[0], '\'"', ''));
                        }
                    }

                    if (!isset($put[$pipe])) {
                        return;
                    }

                    $linePrefix = sprintf("[%s] %s: ", strtoupper($pipe[0]), $k);
                    $isSame = isset($lastWrite) && $lastWrite[0] === $this && $lastWrite[1] === $pipe;

                    if (isset($lastWrite) && !$isSame && $lastWrite[2] !== "\n") {
                        $put[$lastWrite[1]]->write("\n");
                    }
                    if (!isset($lastWrite) || !$isSame || $lastWrite[2] === "\n") {
                        $put[$pipe]->write($linePrefix);
                    }

                    $put[$pipe]->write(strtr(substr($content, 0, -1), ["\n" =>  "\n" . $linePrefix]) . $content[-1]);

                    $lastWritePerPut[$put[$pipe]] = $lastWrite = [$this, $pipe, $content[-1]];
                })->bindTo($processes[$k])
            );
        }

        $activeProcesses = $processes;
        $interval = (float) ($input->getOption('interval') ?? $this->config['status-interval']);
        
        while (count($activeProcesses) > 0) {
            usleep($interval * 1E6);

            foreach ($activeProcesses as $k => $p) {
                if (!$p->isRunning()) {
                    unset($activeProcesses[$k]);
                }
            }
        }

        foreach ($lastWritePerPut as $p) {
            $p = $lastWritePerPut[$p];
            if ($p[2] !== "\n") {
                $put[$p[1]]->write("\n");
            }
        }

        $status = 0;
        foreach ($processes as $k => $p) {
            $pStatus = $p->getExitCode();
            if ($pStatus === null) {
                $status++;
                $put['err']->writeln(sprintf('Was terminated: %s', $p->getCommandLine()));
            } elseif ($pStatus !== 0) {
                $status++;
                $put['err']->writeln(sprintf('Returned with status code %d: %s', $pStatus, $p->getCommandLine()));
            }
        }

        return $status;
    }
}
