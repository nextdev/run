<?php
namespace nextdev\Run\Command;

use SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Finder\Finder;
use nextdev\Run\Configuration\Configuration;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

abstract class BaseCommand extends Command
{
    const ENV_CONFIG = 'RUN_CONFIG_FILES';

    /**
     * Parsed configuration
     */
    protected array $config;

    public function configure(): void
    {
        $this->setDefinition([
            new InputOption(
                'config',
                'c',
                InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
                'Config file to read',
            ),
        ]);
    }

    protected function getErrput(
        OutputInterface $output
    ): OutputInterface {
        return $output instanceof ConsoleOutputInterface ? $output->getErrorOutput() : $output;
    }

    /**
     * Determine the list of config files to be used.
     *
     * @return SplFileInfo[]
     */
    protected function getConfigFiles(
        InputInterface $input,
        OutputInterface $output
    ): iterable {
        $configFiles = $input->getOption('config');

        if (!isset($configFiles)) {
            $envConfigFiles = getenv($this::ENV_CONFIG);
            if ($envConfigFiles !== false) {
                $configFiles = preg_split(
                    '/' . preg_quote(PATH_SEPARATOR, '/') . '|' . preg_quote(';') . '/',
                    $envConfigFiles
                );
            }
        }

        $configFiles = array_map(
            fn($v) => new SplFileInfo($v),
            array_filter($configFiles ?? [], fn($v) => $v !== "")
        );

        if (\count($configFiles) === 0) {
            $configFiles = (new Finder())->files()->name(['run.json', 'run.yaml'])->depth(0)->in(getcwd());

            if (\count($configFiles) === 0) {
                throw new RuntimeException(sprintf(
                    'Could not locate config files. '.
                    'Set environment variable %s to the location of your config files or set the --config option.',
                    $this::ENV_CONFIG
                ));
            } else {
                $this->getErrput($output)->writeln(sprintf(
                    'Guessed config to be located at "%s"...',
                    implode('", "', iterator_to_array($configFiles))
                ));
            }
        }

        return $configFiles;
    }

    /**
     * Set $this->config to the parsed configuration.
     */
    protected function readConfig(
        InputInterface $input,
        OutputInterface $output
    ): void {
        $configs = [];

        foreach ($this->getConfigFiles($input, $output) as $f) {
            if (!$f->isFile() || !$f->isReadable()) {
                throw new RuntimeException(sprintf(
                    'Config file "%s" is not available.',
                    $f->getFilename()
                ));
            }

            $fileExension = $f->getExtension();
            if ($fileExension === 'yml' || $fileExension === 'yaml') {
                $configs[] = Yaml::parseFile($f->getFilename());
            } elseif ($fileExension === 'json') {
                $configs[] = \json_decode(\file_get_contents($f->getFilename()), true, 512, JSON_THROW_ON_ERROR);
            }
        }

        try {
            $this->config = (new Processor())->processConfiguration(new Configuration(), $configs);
        } catch (InvalidConfigurationException $e) {
            throw new RuntimeException(
                'At least one of your configuration files is invalid.' . PHP_EOL . $e->getMessage()
            );
        }
    }
}
