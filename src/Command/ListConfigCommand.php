<?php
namespace nextdev\Run\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ListConfigCommand extends BaseCommand
{
    protected static $defaultName = "list-config";

    public function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        foreach ($this->getConfigFiles($input, $output) as $f) {
            $output->writeln($f->getFilename());
        }

        return 0;
    }
}
